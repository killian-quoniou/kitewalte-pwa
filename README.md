# README

## Sujet de notre projet

L’objectif de notre projet est de créer un site internet recensant, à un seul endroit et pour une ville précise (dans notre cas Saint-Étienne), les listes des lieux culturels (par ex : Cinéma, Musées, Théâtre…), des services municipaux et privés (par ex : Parking, STAS, etc.), des lieux de restauration ou encore des magasins…

Le site se découpe en deux parties. Une première dans laquelle on retrouve les différentes listes précédemment citées et une deuxième dans laquelle on retrouve une carte (basée sur OpenStreetMap) interactive qui permet de localiser tous les lieux listés dans la première partie du site et de les trier avec un système de paramètres de recherche (par exemple : que les restaurants ; que les magasins, etc.).


On retrouve également sur notre application :

- Un système de compte utilisateur, donc par conséquent d'inscription et de connexion. Un utilisateur, par défaut, n'a pas de droit sur le site mais il est possible qu'il recoive les droits administrateurs par un ajout direct, en base de données, des développeurs.
- Un système de gestion (ajout, modification et suppression) des lieux et points d'intérêts pour les administrateurs.

## Instructions pour compiler, lancer et tester notre projet

Il y a deux serveurs à exécuter pour pouvoir lancer notre projet.

**Prérequis :** 
- Votre IDE doit comporter les plugins nécessaires au bon fonctionnement de Spring Boot et de Vue.JS.
- Avoir téléchargé le projet à partir de Gitlab.
- Avoir le package "node" installé.

Le premier, qui est un serveur Spring (backend) se lance de cette manière :

- Étape 1 : Ouvrir votre IDE depuis le répertoire "Spring" du projet.
- Étape 2 : Lancer le serveur Spring "babetlife" depuis le "Spring Boot Dashboard" (pour VS Code) ou équivalent en fonction de votre IDE.

Le second, qui est un serveur Node.js, qui permet de faire fonctionner le frontend Vue.JS, se lance de cette manière :

- Étape 1 : Depuis votre IDE, utiliser la fonctionnalité permettant d'ajouter un dossier à votre répertoire de travail actuel.
- Étape 2 : Ajouter le dossier "VueJS".
- Étape 3 : Ouvrir un terminal, soit depuis votre IDE, soit depuis votre ordinateur.
- Étape 4 : Positionnez vous dans le répertoire "babetlife" du dossier "VueJS".
- Étape 5 : Tapez la commande "npm install", puis exécuter là.
- Étape 6 : Tapez la commande "npm run serve", puis exécuter là.

Les serveurs Spring et Node.js sont maintenant fonctionnels.

Pour utiliser notre site internet sur votre ordinateur, il vous suffit de vous rendre sur l'URL : [http://localhost:8081/](http://localhost:8081/).
Pour utiliser notre site internet sur votre smartphone, il vous suffit de vous connecter au même réseau Wi-fi que votre ordinateur et vous rendre sur l'addresse "Network", visible sur le terminal sur lequel le serveur Node.js est lancé.


## Description de l’architecture

Tout d'abord, il y a deux versions de notre application :
- Une version Spring (backend) combinée à Thymeleaf (frontend).
- Une version Spring (backend) combinée à Vue.JS (frontend).

La base du projet est la même pour les deux parties :
- Des entités JPA, ainsi que leur "Repositories" sont créés.
- Pour ajouter dans une base de données "H2 Database" les informations liées aux entités JPA, nous utilisons une classe "Service".

Pour la première version, Spring/Thymeleaf:
- Un Controller (non-rest &rarr; @Controller) renvoie aux pages HTML les données correspondantes aux attributs des entités qui sont nécessaires sur ces pages. Elles sont affichées par l'intermédiaire des attributs Thymeleaf, présents dans les balises HTML.
- Dans cette version, il n'est pas question d'ajouter, modifier ou encore supprimer des entités par l'intermédiaire de formulaires.
Pour la deuxième version, Spring/Vue.JS:
- Un REST Controller (@RestController), lorsque l'on fait une requête GET à une URL précise, renvoie les données brutes correspondantes aux attributs des entités sous forme de fichiers JSON, comme une API REST classique. Tout cela, aux différentes Vues de Vue.JS. Ces dernières interprétent les données et les affichent.
- Dans cette version, il y a des administrateurs et donc un système d'ajout, modification et suppression d'entités par l'intermédiaire de formulaires. Ces derniers sont envoyés depuis le serveur de Vue.JS à celui de Spring et les changements sont interprétés par le Rest Controller qui agit directement sur la base de données.

## Instance de notre service
Nous n'avons pas d'instance de notre service. En effet, nous avons considéré que nous le ferions seulement si nous avions fini l'intégralité de nos idées de base, et idées bonus comprises, car nous pouvons déjà tester le site internet en local.

## Sources externes
Outre la documention des différentes technologies, nous avons utilisé du code externe pour la partie Inscription/Connexion de l'application.
Voici les liens des différentes sources :
- https://www.bezkoder.com/spring-boot-vue-js-authentication-jwt-spring-security/
- https://www.bezkoder.com/spring-boot-jwt-authentication/
- https://www.springframework.guru/using-the-h2-database-console-in-spring-boot-with-spring-security/

Les deux premiers articles ont été rédigés par "bezkoder" pour le site https://www.bezkoder.com.
Le dernier article a été rédigé par "jt" pour le site https://springframework.guru.

Malgré nos recherches et études pour intégrer la fonctionnalité Inscription/Inscription, notre manque de connaissance sur Spring Boot Security nous a rapidement bloqués. Le manque d'un cours appronfondi dédié sur cette partie de Spring et le vaste champ d'informations étaient handicapant.

Malgré l'utilisation de ces sources externes, nous avons pris le temps de comprendre le principe et le code utilisés. Chaque membre de l'équipe a pris le temps d'étudier l'utilité de chaque fonction et principe de Spring Boot Security.

