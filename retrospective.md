# Rétrospective du groupe
Ce document a pour objectif de fournir une rétrospective générale du projet de "Programmation Web Avancée".


## Retour de projet

Premièrement, concernant notre projet (vous trouverez le rappel de notre sujet dans le document "README.md"), le résultat obtenu coincide avec nos attentes et la vision que l'on imaginait.

Comparé à notre idée de base, nous n'avons seulement pas eu le temps de nous pencher sur l'utilisation d'API externes qui nous auraient permis de mettre à jour en temps réel les informations des différents lieux de Saint-Étienne. 

Cependant, nous avons finalement considéré que l'objectif principal du projet n'était pas là et que ce serait un bonus de le réaliser.

Nous sommes fiers de ce que nous avons réalisé pour un premier projet "fullstack" en Spring et Vue.JS.


## Organisation du travail

Concernant l'organisation de notre projet, nous avons utilisés les outils GitLab pour la gestion du projet entre les différents membres, et Jira Software pour la gestion des tickets. Ce dernier est plus pratique que la gestion des tickets de GitLab, d'où notre choix.

Pour le bon déroulement du projet, nous avons fonctionné en Sprint, de une ou deux semaines selon la quantité de travail.

Quant au travail en lui-même, chaque fonctionnalité faisait l'objet d'un ticket. Chaque ticket devait être traité sur une branche créée et dédiée à cette fonctionnalité.

Une fois le travail sur une branche terminée, elle devait faire l'objet d'une "Merge Request", comportant un "Reviewer" et un "Assigner". 

Le rôle du "Reviewer" est de relire le travail réalisé et de demander des corrections si nécessaire. 

Le rôle de l'"Assigner" est également de relire le travail réalisé, demander des corrections si nécessaire mais aussi et surtout de valider la fusion ("merge") une fois que le "Reviewer" et lui-même ont fini de corriger le travail qui a été fait.

## Les difficultés rencontrées

Comme dans tout projet, nous avons rencontré plusieurs difficultés.

Pour la première partie du projet, comportant notamment Spring et Thymeleaf, nous n'avons pas eu d'obstacle particulier.

Cependant, concernant la partie "Single Page App" avec Vue.JS, nous avons fait dû faire face à plusieurs problèmes.

Le premier était un problème de lancement du serveur Node.js, dû à une erreur provoquée par ESLint. En cause un fichier du même nom, a priori mal configuré. Malgré les tentatives de résolution pendant de nombreux jours, on a finalement choisi de retirer ce fichier du build du serveur et le problème a été résolu.

Le second a été sur le système utilisateur. Initialement, cette partie n'était pas prévue, mais suite à une modification du sujet, on a dû l'intégrer en cours de projet, avec toutes les complications de conception qui suivent (Spring Boot Security et l'authentification avec JSON Web Token).

D'autres problèmes mineurs nous sont arrivés, tels que la compréhension du routage dynamique ou encore l'accès à l'application à partir d'un smartphone (pour la version avec Vue.JS).

## L'expérience gagnée

Étant en études orientées développement, c'est très intéressant de travailler sur des technologies Web du moment qui sont beaucoup utilisées en entreprise.

Un autre point intéressant est l'utilisation rigoureuse de GitLab sur plusieurs mois, qui nous a permis de nous améliorer, notamment pour notre prochain stage.

Un dernier point, la gestion de projet. Le fait d'être en équipe de 4 sur plusieurs mois nous a permis de nous améliorer dans nos capacités d'organisation, de compréhension et d'adaptation pour toujours avancer malgré les problèmes.

## Les points à améliorer

Le point principal à améliorer selon nous est notre capacité à apprendre de nouveaux concepts en totale autonomie.

En effet, il a été compliqué de comprendre comment fonctionne le système d'authentification avec JSON Web Token sans que ce concept n'ait été abordé en cours.

Le second point important est de suivre davantage les conventions de clean code que ce que nous avons fait pour ce projet. En effet, nous avons globalement respecté ces dernières mais pas l'intégralité du temps, avec les "Services", par exemple, que nous avons réunis en un seul fichier au lieu d'un par entité.

## Les points positifs à retenir et à garder

Nous sommes fiers d'avoir su instaurer et garder une gestion de projet efficace tout au long de ce dernier. De par des réunions ou de par la gestion avec GitLab et Jira Software.

Un autre point positif est la complémentarité des membres du groupe. Lorsque qu'un membre du groupe avait des problèmes sur son ticket, un autre venait l'aider et cela nous a rendu efficace tout au long du projet.

Dans l'ensemble, le projet s'est très bien déroulé et nous reprendrons la même organisation pour nos prochains projets.