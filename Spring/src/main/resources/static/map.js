var Map = ol.Map;
var OSM = ol.source.OSM;
var Overlay = ol.Overlay;
var View = ol.View;
var toStringHDMS = ol.coordinate.toStringHDMS;
var TileLayer = ol.layer.Tile;
var fromLonLat = ol.proj.fromLonLat;
var toLonLat = ol.proj.toLonLat;
var TileJSON = ol.source.TileJSON;

var mapWindowWidth = window.innerWidth;

const saintEtienneCoo = {
  x: 488392,
  y: 5691067
}

const layer = new TileLayer({
  source: new OSM(),
});

const map = new Map({
  layers: [layer],
  target: 'map',
  view: new View({
    center: [saintEtienneCoo.x, saintEtienneCoo.y],
    zoom: 12,
  }),
});

var pos = [];
places.forEach(l => {
    var popup = new Overlay({
        element: document.getElementById('popup'+l.id)
    });
    pos.push({
        id: l.id,
        position: fromLonLat([l.longitude, l.latitude]),
        popup: popup,
        name: l.name,
        categoryID: l.categoryID,
        address: l.address,
        numTel: l.numTel,
        notation: l.notation
    });
    map.addOverlay(popup);
});

var markers = [];
pos.forEach(p => {
    var markerElem = document.getElementById('marker'+p.id);
    var marker = new Overlay({
        position: p.position,
        positioning: 'center-center',
        element: markerElem,
        stopEvent: false,
    });
    markerElem.onclick =function (evt) {
        const element = p.popup.getElement();
        const coordinate = p.position;

        $(".popup").popover('dispose');
        p.popup.setPosition(coordinate);
        $(element).popover({
            container: element,
            placement: 'top',
            animation: false,
            trigger: "focus",
            html: true,
            title: p.name+`<a href="#" class="close">&times;</a>`,
            content: `
              <p>
                <span class="underline">Adresse :</span> `+p.address+`<br />
                <span class="underline">Tel :</span> `+p.numTel+`<br />
                <span class="underline">Notation :</span> `+p.notation+`
              </p>
              <p class="more-info">
                <a href="/lieu/`+p.id+`" class="btn btn-info">
                  Plus d'informations
                </a>
              </p>`,
        });
        $(document).on("click", ".close" , function(){
          $(this).parents(".popover").popover('dispose');
          return false;
        });
        $(element).popover('show');
    };
    markers.push({
      overlay: marker,
      categoryID: p.categoryID,
      popup: p.popup.getElement()
    });
    map.addOverlay(marker);
});

function geolocationSuccess(position) {
  console.log(position.longitude+","+position.latitude);
  let myLocationElem = document.getElementById("marker-my-pos");
  let myLocation = new Overlay({
      position: fromLonLat([position.coords.longitude, position.coords.latitude]),
      positioning: 'center-center',
      element: myLocationElem,
      stopEvent: false,
  });
  map.addOverlay(myLocation);
}

function geolocationError() {
  return;
}

if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
}

function toggleCategory(categoryID) {
  var checkbox = document.getElementById('category'+categoryID);
  if (checkbox.checked) {
    markers.forEach(m => {
      if (m.categoryID == categoryID) {
        map.addOverlay(m.overlay);
      }
    });
  } else {
    markers.forEach(m => {
      if (m.categoryID == categoryID) {
        $(m.popup).popover('dispose');
        map.removeOverlay(m.overlay);
      }
    });
  }
}

function open_filter_menu() {
  document.getElementById("filterMenu").style.backgroundColor = "rgba(28,28,28,0.5)";
  document.getElementById("filterMenu").style.width = "100%";
  document.getElementById("filterMenu").style.top = "0";

  document.getElementById("open_filter_menu").style.display = "none";
  document.getElementById("open_menu_sandwich").style.display = "none";

  document.getElementById("filter").style.display = "flex";

  document.getElementById("close_filter_menu").style.display = "initial";
}

function close_filter_menu() {
  document.getElementById("filterMenu").style.backgroundColor = "rgba(0,0,0,0)";
  if (window.matchMedia("(max-width: 1080px)").matches){
    document.getElementById("filterMenu").style.width = "0";
    document.getElementById("filterMenu").style.top = "25vw";
    document.getElementById("close_filter_menu").style.display = "none";
    document.getElementById("open_filter_menu").style.display = "initial";
    document.getElementById("open_menu_sandwich").style.display = "initial";

    document.getElementById("filter").style.display = "none";
  }else{
    document.getElementById("filterMenu").style.width = "30%";
    document.getElementById("close_filter_menu").style.display = "none";
    document.getElementById("open_filter_menu").style.display = "none";

    document.getElementById("filter").style.display = "flex";
  }
}

window.onresize = () => {
  if (mapWindowWidth != window.innerWidth) {
    close_menu_phone();
    close_filter_menu();
    mapWindowWidth = window.innerWidth;
  }
};