
var windowWidth = window.innerWidth;

function open_menu_phone() {
  document.getElementById("navBar").style.backgroundColor = "rgba(28,28,28,0.5)";
  document.getElementById("navBar").style.width = "100%";

  if (document.getElementById("open_filter_menu") != undefined) {
    document.getElementById("open_filter_menu").style.display = "none";
  }
  document.getElementById("open_menu_sandwich").style.display = "none";

  let navbarLis = document.getElementsByClassName("navbarLi");
  for (i = 0; i < navbarLis.length; i++) {
    navbarLis[i].style.display = "flex";
  }
  let navbarAs = document.getElementsByClassName("navbarA");
  for (i = 0; i < navbarAs.length; i++) {
    navbarAs[i].style.display = "flex";
  }

  document.getElementById("close_menu_sandwich").style.display = "initial";
}

function close_menu_phone() {
  document.getElementById("navBar").style.backgroundColor = "rgba(0,0,0,0)";
  if (window.matchMedia("(max-width: 1080px)").matches) {
    document.getElementById("navBar").style.width = "0";
    document.getElementById("close_menu_sandwich").style.display = "none";
    if (document.getElementById("open_filter_menu") != undefined) {
        document.getElementById("open_filter_menu").style.display = "initial";
      }
    document.getElementById("open_menu_sandwich").style.display = "initial";
    let navbarLis = document.getElementsByClassName("navbarLi");
    for (i = 0; i < navbarLis.length; i++) {
      navbarLis[i].style.display = "none";
    }
    let navbarAs = document.getElementsByClassName("navbarA");
    for (i = 0; i < navbarAs.length; i++) {
      navbarAs[i].style.display = "none";
    }
  } else {
    document.getElementById("navBar").style.width = "100%";
    document.getElementById("close_menu_sandwich").style.display = "none";
    document.getElementById("open_menu_sandwich").style.display = "none";
    let navbarLis = document.getElementsByClassName("navbarLi");
    for (i = 0; i < navbarLis.length; i++) {
      navbarLis[i].style.display = "flex";
    }
    let navbarAs = document.getElementsByClassName("navbarA");
    for (i = 0; i < navbarAs.length; i++) {
      navbarAs[i].style.display = "flex";
    }
  }
}

window.onresize = () => {
  if (windowWidth != window.innerWidth) {
    close_menu_phone();
    windowWidth = window.innerWidth;
  }
};
