setTimeout(function () {
    window.location.href = '/';
}, 7000);

var seconds = 5;
var counter = document.getElementById("counter");

setInterval(function (){
    if(seconds > 0)
        seconds -= 1;
    if(seconds > 1)
        counter.innerHTML = seconds + " secondes";
    else
        counter.innerHTML = seconds + " seconde";
}, 1000);