let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    const slides = document.getElementsByClassName("slides");
    const dots = document.getElementsByClassName("dot");
    if (n > slides.length || n == 1) {
        slideIndex = 1;
    }
    if (n < 1 || n == 5) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "flex";
    slides[slideIndex-1].style.justifyContent = "center";
    slides[slideIndex-1].style.alignItems = "center";
    slides[slideIndex-1].style.flexWrap = "wrap";
    dots[slideIndex-1].className += " active";
}