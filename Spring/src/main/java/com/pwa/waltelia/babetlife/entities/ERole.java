package com.pwa.waltelia.babetlife.entities;

public enum ERole {
    ROLE_USER,
    ROLE_OWNER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
