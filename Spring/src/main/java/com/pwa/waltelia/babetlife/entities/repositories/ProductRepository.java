package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Menu;
import com.pwa.waltelia.babetlife.entities.Product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("SELECT p FROM Product AS p WHERE p.menu = :m")
    public List<Product> getProductsByMenu(Menu m);

}
