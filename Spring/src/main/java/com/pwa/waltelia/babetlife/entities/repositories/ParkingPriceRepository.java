package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.ParkingPrice;
import com.pwa.waltelia.babetlife.entities.Parking;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;

public interface ParkingPriceRepository extends CrudRepository<ParkingPrice, Long> {

    @Query("SELECT t FROM ParkingPrice AS t WHERE t.parking = :p")
    public List<ParkingPrice> getPriceOfParking(Parking p);
}
