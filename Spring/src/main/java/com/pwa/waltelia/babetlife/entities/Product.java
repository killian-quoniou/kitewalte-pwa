package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Menu menu;
    private String name;
    private String type;
    private double price;

    public Product(Menu menu, String name, String type, double price) {
        this.menu = menu;
        this.name = name;
        this.type = type;
        this.price = price;
    }

}
