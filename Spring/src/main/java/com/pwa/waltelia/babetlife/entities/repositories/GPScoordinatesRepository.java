package com.pwa.waltelia.babetlife.entities.repositories;

import com.pwa.waltelia.babetlife.entities.GPScoordinates;

import org.springframework.data.repository.CrudRepository;

public interface GPScoordinatesRepository extends CrudRepository<GPScoordinates,Long> {
    
}
