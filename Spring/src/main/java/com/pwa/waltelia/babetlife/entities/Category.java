package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private boolean isDiscovered;
    
    public Category(String title, boolean isDiscovered) {
        this.title = title;
        this.isDiscovered = isDiscovered;
    }
    
}
