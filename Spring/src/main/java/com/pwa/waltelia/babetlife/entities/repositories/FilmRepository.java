package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Cinema;
import com.pwa.waltelia.babetlife.entities.Film;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FilmRepository extends CrudRepository<Film, Long> {

    @Query("SELECT f FROM Film AS f WHERE f.cinema = :c")
    public List<Film> getAllFilmsByCinema(Cinema c);
}
