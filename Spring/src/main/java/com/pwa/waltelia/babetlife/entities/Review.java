package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Review {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Place place;
    private String comment;
    private double grade;
    private String pseudo;

    public Review(Place place, String comment, double grade, String pseudo) {
        this.place = place;
        this.comment = comment;
        this.grade = grade;
        this.pseudo = pseudo;
    }

}
