package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Store extends Place {

    private String type;

    public Store(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation, String type) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
