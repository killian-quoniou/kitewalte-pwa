package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class RestaurantBar extends Place {

    private int capacity;
    
    public RestaurantBar(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation, int capacity) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

}
