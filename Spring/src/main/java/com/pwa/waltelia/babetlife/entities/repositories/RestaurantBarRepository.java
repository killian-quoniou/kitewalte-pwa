package com.pwa.waltelia.babetlife.entities.repositories;

import com.pwa.waltelia.babetlife.entities.RestaurantBar;

import org.springframework.data.repository.CrudRepository;

public interface RestaurantBarRepository extends CrudRepository<RestaurantBar, Long> {

}
