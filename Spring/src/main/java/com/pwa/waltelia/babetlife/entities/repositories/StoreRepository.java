package com.pwa.waltelia.babetlife.entities.repositories;

import com.pwa.waltelia.babetlife.entities.Store;

import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<Store, Long> {

}
