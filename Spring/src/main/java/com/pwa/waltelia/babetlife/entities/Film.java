package com.pwa.waltelia.babetlife.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Cinema cinema;
    private String title;
    private String type;
    private double price;
    private Date schedule;
    private double time;
    private int numRoom;
    
    public Film(Cinema cinema, String title, String type, double price, Date schedule, double time, int numRoom) {
        this.cinema = cinema;
        this.title = title;
        this.type = type;
        this.price = price;
        this.schedule = schedule;
        this.time = time;
        this.numRoom = numRoom;
    }

}
