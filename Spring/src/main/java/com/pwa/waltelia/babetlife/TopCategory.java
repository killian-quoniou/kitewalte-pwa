package com.pwa.waltelia.babetlife;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Place;

import lombok.Data;

@Data
public class TopCategory {
    String title;
    List<Place> places;
}
