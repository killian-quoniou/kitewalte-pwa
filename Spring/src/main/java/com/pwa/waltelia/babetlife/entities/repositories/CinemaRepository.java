package com.pwa.waltelia.babetlife.entities.repositories;

import com.pwa.waltelia.babetlife.entities.Cinema;

import org.springframework.data.repository.CrudRepository;

public interface CinemaRepository extends CrudRepository<Cinema, Long> {

}
