package com.pwa.waltelia.babetlife;

import java.util.Date;

import javax.inject.Inject;

import com.pwa.waltelia.babetlife.entities.TransportStop;
import com.pwa.waltelia.babetlife.entities.Review;
import com.pwa.waltelia.babetlife.entities.Role;
import com.pwa.waltelia.babetlife.entities.Category;
import com.pwa.waltelia.babetlife.entities.Cinema;
import com.pwa.waltelia.babetlife.entities.ERole;
import com.pwa.waltelia.babetlife.entities.GPScoordinates;
import com.pwa.waltelia.babetlife.entities.Film;
import com.pwa.waltelia.babetlife.entities.Place;
import com.pwa.waltelia.babetlife.entities.Store;
import com.pwa.waltelia.babetlife.entities.Menu;
import com.pwa.waltelia.babetlife.entities.Park;
import com.pwa.waltelia.babetlife.entities.Parking;
import com.pwa.waltelia.babetlife.entities.Product;
import com.pwa.waltelia.babetlife.entities.RestaurantBar;
import com.pwa.waltelia.babetlife.entities.ParkingPrice;
import com.pwa.waltelia.babetlife.entities.repositories.TransportStopRepository;
import com.pwa.waltelia.babetlife.entities.repositories.UserRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ReviewRepository;
import com.pwa.waltelia.babetlife.entities.repositories.RoleRepository;
import com.pwa.waltelia.babetlife.entities.repositories.CategoryRepository;
import com.pwa.waltelia.babetlife.entities.repositories.CinemaRepository;
import com.pwa.waltelia.babetlife.entities.repositories.GPScoordinatesRepository;
import com.pwa.waltelia.babetlife.entities.repositories.FilmRepository;
import com.pwa.waltelia.babetlife.entities.repositories.StoreRepository;
import com.pwa.waltelia.babetlife.entities.repositories.MenuRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ProductRepository;
import com.pwa.waltelia.babetlife.entities.repositories.RestaurantBarRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingPriceRepository;

import org.springframework.stereotype.Service;

@Service
public class InitTestDatabase {

    @Inject
    TransportStopRepository transportStopRepository;
    @Inject
    ReviewRepository reviewRepository;
    @Inject
    CategoryRepository categoryRepository;
    @Inject
    CinemaRepository cinemaRepository;
    @Inject
    FilmRepository filmRepository;
    @Inject
    StoreRepository storeRepository;
    @Inject
    MenuRepository menuRepository;
    @Inject
    ParkRepository parkRepository;
    @Inject
    ParkingRepository parkingRepository;
    @Inject
    ProductRepository productRepository;
    @Inject
    RestaurantBarRepository restaurantBarRepository;
    @Inject
    ParkingPriceRepository parkingPriceRepository;
    @Inject
    GPScoordinatesRepository GPScoordinatesRepository;
    @Inject
    RoleRepository roleRepository;
    @Inject
    UserRepository userRepository;

    public void main() {

        Category categoryPark = new Category("Parcs", true);
        Category categoryStore = new Category("Magasins", true);
        Category categoryParking = new Category("Parkings", false);
        Category categoryCinema = new Category("Cinémas", true);
        Category categoryRestaurantBar = new Category("Restaurants / Bars", true);
        Category categoryPublicTransportStop = new Category("Arrêts transports en commun", false);

        categoryRepository.save(categoryPark);
        categoryRepository.save(categoryStore);
        categoryRepository.save(categoryParking);
        categoryRepository.save(categoryCinema);
        categoryRepository.save(categoryRestaurantBar);
        categoryRepository.save(categoryPublicTransportStop);

        fillPark(categoryPark);
        fillStore(categoryStore);
        fillParking(categoryParking);
        fillCinema(categoryCinema);
        fillRestaurantBar(categoryRestaurantBar);
        fillPublicTransportStop(categoryPublicTransportStop);

        fillRoles();

    }

    private void fillRoles() {
        Role rUser = new Role(ERole.ROLE_USER);
        Role rOwner = new Role(ERole.ROLE_OWNER);
        Role rModerator = new Role(ERole.ROLE_MODERATOR);
        Role rAdmin = new Role(ERole.ROLE_ADMIN);
        roleRepository.save(rUser);
        roleRepository.save(rOwner);
        roleRepository.save(rModerator);
        roleRepository.save(rAdmin);
    }

    private void fillReview(Place place, String comment, double review, String pseudo) {
        Review avis = new Review(place, comment, review, pseudo);
        reviewRepository.save(avis);
    }

    private void fillPublicTransportStop(Category categoryPublicTransportStop) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.441389, 4.386389);
        GPScoordinates cooGPS2 = new GPScoordinates(45.442778, 4.399722);
        GPScoordinates cooGPS3 = new GPScoordinates(45.419722, 4.411389);
        TransportStop transportStop1 = new TransportStop(categoryPublicTransportStop, "Jean Jaurès",
                "Place Jean Jaurès", "08 10 34 23 42", cooGPS1, "24h/24h", 5, "T1,T2");
        TransportStop transportStop2 = new TransportStop(categoryPublicTransportStop, "Châteaucreux",
                "Gare de Châteaucreux", "08 10 34 23 42", cooGPS2, "24h/24h", 4.2,
                "T2,T3,M3,M4,M5,11,12,14,16,29");
        TransportStop transportStop3 = new TransportStop(categoryPublicTransportStop, "Parc de l'Europe",
                "6 Bd Alexandre de Fraissinette", "08 10 34 23 42", cooGPS3, "24h/24h", 2.5, "M6,M4");

        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        transportStopRepository.save(transportStop1);
        transportStopRepository.save(transportStop2);
        transportStopRepository.save(transportStop3);
        fillReview(transportStop1, "Très joli", 4.5, "Kelkun kontan");
        fillReview(transportStop1, "En retard !!", 1.5, "Kelkun pa kontan");
    }

    private void fillRestaurantBar(Category categoryRestaurantBar) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.433889, 4.387778);
        GPScoordinates cooGPS2 = new GPScoordinates(45.439167, 4.387778);
        GPScoordinates cooGPS3 = new GPScoordinates(45.442222, 4.398611);
        RestaurantBar restauBar1 = new RestaurantBar(categoryRestaurantBar, "L'Escargot d'Or", "5 Cr Victor Hugo",
                "+33477412404", cooGPS1, "Mar-Sam : 9h-19h", 4.4, 35);
        RestaurantBar restauBar2 = new RestaurantBar(categoryRestaurantBar, "Le Glasgow", "3 Pl. Hôtel de ville",
                "+33477328399", cooGPS2, "Lun-Jeu : 7h30-20h;Ven-Sam : 7h30-22h", 4.2, 20);
        RestaurantBar restauBar3 = new RestaurantBar(categoryRestaurantBar, "la loco", "29 Av. Denfert Rochereau",
                "+33477386754", cooGPS3, "Lun-Jeu : 12h-15h,19h-23h;Sam : 19h-23h;Dim : 12h-15h", 4.3, 45);
        
        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        restaurantBarRepository.save(restauBar1);
        restaurantBarRepository.save(restauBar2);
        restaurantBarRepository.save(restauBar3);
        fillMenu(restauBar1, restauBar2, restauBar3);
        fillReview(restauBar1, "Très bon repas", 4.7, "Une fine bouche");
        fillReview(restauBar2, "Acceptable mais sans plus", 2.4, "Une bouche trop fine");
    }

    private void fillMenu(RestaurantBar restauBar1, RestaurantBar restauBar2, RestaurantBar restauBar3) {
        Menu menu11 = new Menu(restauBar1, 10);
        Menu menu12 = new Menu(restauBar1, 15);
        Menu menu21 = new Menu(restauBar2, 15);
        Menu menu22 = new Menu(restauBar2, 22);
        Menu menu31 = new Menu(restauBar3, 9);
        Menu menu32 = new Menu(restauBar3, 18);
        menuRepository.save(menu11);
        menuRepository.save(menu12);
        menuRepository.save(menu21);
        menuRepository.save(menu22);
        menuRepository.save(menu31);
        menuRepository.save(menu32);
        fillProduct(menu11, menu12, menu21, menu22, menu31, menu32);
    }

    private void fillProduct(Menu menu11, Menu menu12, Menu menu21, Menu menu22, Menu menu31, Menu menu32) {
        Product product111 = new Product(menu11, "Salade", "Entrée", 3.5);
        Product product112 = new Product(menu11, "Steak frites", "Plat", 10.5);
        Product product121 = new Product(menu12, "Entrecote", "Plat", 11.5);
        Product product122 = new Product(menu12, "Boule de glace", "Dessert", 5.5);
        Product product211 = new Product(menu21, "Poisson", "Plat", 11);
        Product product212 = new Product(menu21, "Fromage du terroir", "Fromage", 6.5);
        Product product221 = new Product(menu22, "Bouteille d'eau", "Boisson", 3.5);
        Product product222 = new Product(menu22, "Burger", "Plat", 13.5);
        Product product311 = new Product(menu31, "Soda", "Boisson", 5.5);
        Product product312 = new Product(menu31, "Jardinière de légumes", "Plat", 12.5);
        Product product321 = new Product(menu32, "Charcuterie", "Entrée", 7.5);
        Product product322 = new Product(menu32, "Café", "Digestif", 15);
        productRepository.save(product111);
        productRepository.save(product112);
        productRepository.save(product121);
        productRepository.save(product122);
        productRepository.save(product211);
        productRepository.save(product212);
        productRepository.save(product221);
        productRepository.save(product222);
        productRepository.save(product311);
        productRepository.save(product312);
        productRepository.save(product321);
        productRepository.save(product322);
    }

    private void fillCinema(Category categoryCinema) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.438889, 4.401667);
        GPScoordinates cooGPS2 = new GPScoordinates(45.441667, 4.385);
        GPScoordinates cooGPS3 = new GPScoordinates(45.445833, 4.333889);
        Cinema cine1 = new Cinema(categoryCinema, "Le Méliès Saint-François", "8 Rue de la Valse", "+33477327696",
                cooGPS1, "15h-02h", 4.3, 5);
        Cinema cine2 = new Cinema(categoryCinema, "Megarama Jean Jaurès", "2 Rue Praire, Pl. Jean Jaurès",
                "+33477323847", cooGPS2, "14h-02h30", 3.7, 10);
        Cinema cine3 = new Cinema(categoryCinema, "Cine-Lerpt", "5 Rue Eugène Bonnardel", "+33477902213",
                cooGPS3, "16h-00h", 3.8, 3);
        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        cinemaRepository.save(cine1);
        cinemaRepository.save(cine2);
        cinemaRepository.save(cine3);
        fillFilm(cine1, cine2, cine3);
        fillReview(cine2, "Salle correct", 3.6, "Un Jean");
        fillReview(cine3, "Salle dégeu, du popcorn partout", 1.4, "Un Jean pa cool");
    }

    private void fillFilm(Cinema cine1, Cinema cine2, Cinema cine3) {
        Film film11 = new Film(cine1, "Dune", "Science-Fiction", 4.2, new Date(2021, 9, 22, 21, 30), 2.5, 2);
        Film film12 = new Film(cine1, "Harry Potter et la coupe de feu", "Fantastique", 3.5,
                new Date(2021, 9, 24, 16, 45), 2.25, 4);
        Film film21 = new Film(cine2, "James Bond - Skyfall", "Action", 4.6, new Date(2021, 9, 23, 17, 00), 2.5, 7);
        Film film22 = new Film(cine2, "Fast & Furious 7", "Action", 3.8, new Date(2021, 9, 24, 21, 00), 2, 9);
        Film film31 = new Film(cine3, "Dune", "Science-Fiction", 4.9, new Date(2021, 9, 22, 16, 30), 2.5, 2);
        Film film32 = new Film(cine3, "Fast & Furious 7", "Action", 3.8, new Date(2021, 9, 25, 23, 30), 2, 1);
        filmRepository.save(film11);
        filmRepository.save(film12);
        filmRepository.save(film21);
        filmRepository.save(film22);
        filmRepository.save(film31);
        filmRepository.save(film32);
    }

    private void fillParking(Category categoryParking) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.438611, 4.387778);
        GPScoordinates cooGPS2 = new GPScoordinates(45.440833, 4.385);
        GPScoordinates cooGPS3 = new GPScoordinates(45.435278, 4.386667);
        Parking parking1 = new Parking(categoryParking, "Q-Park Hotel de ville", "13 Pl. de l'Hôtel de Ville",
                "+33477334357", cooGPS1, "24h/24h", 3.7, 325);
        Parking parking2 = new Parking(categoryParking, "Parking Saint-Etienne Jean Jaures - EFFIA", "Pl. Jean Jaurès",
                "+33806000115", cooGPS2, "24h/24h", 3.7, 418);
        Parking parking3 = new Parking(categoryParking, "parking des Ursules", "42000 Pl. des Ursules", "",
                cooGPS3, "24h/24h", 2.9, 150, parking1);
        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        parkingRepository.save(parking1);
        parkingRepository.save(parking2);
        parkingRepository.save(parking3);
        fillParkingPrice(parking1, parking2, parking3);
        fillReview(parking3, "Beaucoup de place pas chère", 4.9, "Un radin");
        fillReview(parking3, "Abruti qui prend 2 places t'abuse sérieux", 2.4, "Un gars qui exprime sa colère");
    }

    private void fillParkingPrice(Parking p1, Parking p2, Parking p3) {
        ParkingPrice tarifParking11 = new ParkingPrice(p1, 0.5, 0);
        ParkingPrice tarifParking12 = new ParkingPrice(p1, 1, 2.5);
        ParkingPrice tarifParking13 = new ParkingPrice(p1, 3, 2.8);

        ParkingPrice tarifParking21 = new ParkingPrice(p2, 0.5, 0.5);
        ParkingPrice tarifParking22 = new ParkingPrice(p2, 1, 1.8);
        ParkingPrice tarifParking23 = new ParkingPrice(p2, 3, 2.2);

        ParkingPrice tarifParking31 = new ParkingPrice(p3, 1, 1);
        ParkingPrice tarifParking32 = new ParkingPrice(p3, 3, 1.5);
        ParkingPrice tarifParking33 = new ParkingPrice(p3, 7, 2.6);

        parkingPriceRepository.save(tarifParking11);
        parkingPriceRepository.save(tarifParking12);
        parkingPriceRepository.save(tarifParking13);
        parkingPriceRepository.save(tarifParking21);
        parkingPriceRepository.save(tarifParking22);
        parkingPriceRepository.save(tarifParking23);
        parkingPriceRepository.save(tarifParking31);
        parkingPriceRepository.save(tarifParking32);
        parkingPriceRepository.save(tarifParking33);
    }

    private void fillStore(Category categoryStore) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.411111, 4.374444);
        GPScoordinates cooGPS2 = new GPScoordinates(45.473611, 4.343056);
        GPScoordinates cooGPS3 = new GPScoordinates(45.477222, 4.357778);
        Store store1 = new Store(categoryStore, "Géant Casino",
                "Centre Commercial Chem. de la Croix de l'Orme", "04 77 81 14 00", cooGPS1,
                "Lun-Sam : 7h30-21h;Dim : 8h-20h", 3.3, "Hypermarché");
        Store store2 = new Store(categoryStore, "Auchan Villars", "Chemin de Montravel", "04 77 79 44 00",
                cooGPS2, "Lun-Sam : 8h-21h30;Dim : 8h-12h30", 3.8, "Hypermarché");
        Store store3 = new Store(categoryStore, "Leroy Merlin", "3 Avenue Pierre Mendès France",
                "04 77 92 14 14", cooGPS3, "Lun-Sam : 9h-20h;Dim : 9h-13h", 3.9, "Bricolage");
        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        storeRepository.save(store1);
        storeRepository.save(store2);
        storeRepository.save(store3);
        fillReview(store1, "Grand magasin avec plein de produit", 4.1, "Kelkun");
        fillReview(store3, "ET CA, J'ACHETE !!!", 5, "Jean Marc Généreux");
    }

    private void fillPark(Category categoryPark) {
        GPScoordinates cooGPS1 = new GPScoordinates(45.474444, 4.364444);
        GPScoordinates cooGPS2 = new GPScoordinates(45.450556, 4.371944);
        GPScoordinates cooGPS3 = new GPScoordinates(45.460833, 4.4125);
        Park park1 = new Park(categoryPark, "Parc Jean Marc", "28 Rue de Michard", "", cooGPS1, "24h/24h",
                4.2);
        Park park2 = new Park(categoryPark, "Parc Montaud", "41 Rue de Laharpe", "04 77 33 41 76",
                cooGPS2, "24h/24h", 4.5);
        Park park3 = new Park(categoryPark, "Parc des Sports de Méon", "466 Bd Louis Neltner, 42000 Saint-Étienne",
                "04 77 32 58 93", cooGPS3, "24h/24h", 4.1);
        GPScoordinatesRepository.save(cooGPS1);
        GPScoordinatesRepository.save(cooGPS2);
        GPScoordinatesRepository.save(cooGPS3);
        parkRepository.save(park1);
        parkRepository.save(park2);
        parkRepository.save(park3);
        fillReview(park2, "Vive les arbres !!", 4.2, "L'écolo");
        fillReview(park2, "Des crottes de chiens partout !!", 0.4, "Un type à la chaussure sale");
    }

}
