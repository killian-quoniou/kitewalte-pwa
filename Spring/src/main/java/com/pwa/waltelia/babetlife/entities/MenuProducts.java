package com.pwa.waltelia.babetlife.entities;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MenuProducts {
    private Menu menu;
    private List<Product> products;
}
