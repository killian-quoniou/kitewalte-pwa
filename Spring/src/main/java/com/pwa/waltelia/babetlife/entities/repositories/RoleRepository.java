package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.Optional;

import com.pwa.waltelia.babetlife.entities.ERole;
import com.pwa.waltelia.babetlife.entities.Role;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
