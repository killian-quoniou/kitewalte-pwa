package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Parking extends Place {

    private int capacity;
    @ManyToOne
    private Place associatedPlace;

    public Parking(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation, int capacity, Place associatedPlace) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
        this.capacity = capacity;
        this.associatedPlace = associatedPlace;
    }

    public Parking(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation, int capacity) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
        this.capacity = capacity;
        this.associatedPlace = null;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Place getAssociatedPlace() {
        return associatedPlace;
    }

    public void setAssociatedPlace(Place associatedPlace) {
        this.associatedPlace = associatedPlace;
    }
}
