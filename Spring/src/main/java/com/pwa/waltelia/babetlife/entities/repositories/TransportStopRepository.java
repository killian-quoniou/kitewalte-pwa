package com.pwa.waltelia.babetlife.entities.repositories;

import com.pwa.waltelia.babetlife.entities.TransportStop;

import org.springframework.data.repository.CrudRepository;

public interface TransportStopRepository extends CrudRepository<TransportStop, Long> {

}
