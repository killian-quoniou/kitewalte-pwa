package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@NamedNativeQuery(name = "Place.getRandomDiscovery", query = "SELECT PLACE.* FROM PLACE JOIN CATEGORY ON PLACE.CATEGORY_ID = CATEGORY.ID WHERE CATEGORY.IS_DISCOVERED IS TRUE ORDER BY RAND() LIMIT 5", resultClass = Place.class)
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Category category;
    private String name;
    private String address;
    private String numTel;
    @ManyToOne
    private GPScoordinates cooGPS;
    private String openingHours;
    private double notation;
    
    public Place(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation) {
        this.category = category;
        this.name = name;
        this.address = address;
        this.numTel = numTel;
        this.cooGPS = cooGPS;
        this.openingHours = openingHours;
        this.notation = notation;
    }

}
