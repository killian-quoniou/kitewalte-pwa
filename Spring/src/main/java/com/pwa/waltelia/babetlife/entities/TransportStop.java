package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class TransportStop extends Place {

    private String lines;

    public TransportStop(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation, String lines) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
        this.lines = lines;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

}
