package com.pwa.waltelia.babetlife;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.pwa.waltelia.babetlife.entities.Place;
import com.pwa.waltelia.babetlife.entities.Store;
import com.pwa.waltelia.babetlife.entities.MenuProducts;
import com.pwa.waltelia.babetlife.entities.RestaurantBar;
import com.pwa.waltelia.babetlife.entities.Category;
import com.pwa.waltelia.babetlife.entities.Cinema;
import com.pwa.waltelia.babetlife.entities.Parking;
import com.pwa.waltelia.babetlife.entities.repositories.TransportStopRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ReviewRepository;
import com.pwa.waltelia.babetlife.entities.repositories.CategoryRepository;
import com.pwa.waltelia.babetlife.entities.repositories.CinemaRepository;
import com.pwa.waltelia.babetlife.entities.repositories.FilmRepository;
import com.pwa.waltelia.babetlife.entities.repositories.PlaceRepository;
import com.pwa.waltelia.babetlife.entities.repositories.StoreRepository;
import com.pwa.waltelia.babetlife.entities.repositories.MenuRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ProductRepository;
import com.pwa.waltelia.babetlife.entities.repositories.RestaurantBarRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingPriceRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BabetlifeController {

    @Inject
    InitTestDatabase initTestDatabase;

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    PlaceRepository placeRepository;

    @Inject
    StoreRepository storeRepository;

    @Inject
    ParkRepository parkRepository;

    @Inject
    RestaurantBarRepository restaurantBarRepository;

    @Inject
    CinemaRepository cinemaRepository;

    @Inject
    ParkingRepository parkingRepository;

    @Inject
    ParkingPriceRepository parkingPriceRepository;

    @Inject
    TransportStopRepository transportStopRepository;

    @Inject
    MenuRepository menuRepository;

    @Inject
    ProductRepository productRepository;

    @Inject
    FilmRepository filmRepository;

    @Inject
    ReviewRepository reviewRepository;

    @RequestMapping("/")
    public String getIndex(Model model) {
        model.addAttribute("discover", placeRepository.getRandomDiscovery());
        List<TopCategory> topCategories = new ArrayList<>();
        categoryRepository.findAll().forEach(category -> {
            TopCategory topCategory = new TopCategory();
            topCategory.setTitle(category.getTitle());
            topCategory.setPlaces(placeRepository.getTopOfCategory(PageRequest.of(0, 2), category));
            topCategories.add(topCategory);
        });
        model.addAttribute("categories", topCategories);
        model.addAttribute("allCategories", categoryRepository.getAllCategories());
        return "index.html";
    }

    @RequestMapping("/categorie/{id}")
    public String getCategory(Model model, @ModelAttribute("id") Category c) {
        if (c.getTitle() == null) {
            return "redirect:/";
        }
        TopCategory placesByCateg = new TopCategory();
        placesByCateg.setTitle(c.getTitle());
        placesByCateg.setPlaces(placeRepository.getPlacesOfCategory(c));
        model.addAttribute("placesCateg", placesByCateg);
        model.addAttribute("allCategories", categoryRepository.getAllCategories());
        return "category.html";
    }

    @RequestMapping("/map")
    public String getMap(Model model) {
        model.addAttribute("places", placeRepository.findAll());
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("allCategories", categoryRepository.getAllCategories());
        return "map.html";
    }

    @RequestMapping(value = "/lieu/{id}")
    public String getPlace(Model model, @ModelAttribute("id") Place p) {
        switch (p.getCategory().getTitle()) {
            case "Magasins":
                Store s = storeRepository.findById(p.getId()).get();
                model.addAttribute("place", s);
                break;
            case "Parcs":
                model.addAttribute("place", parkRepository.findById(p.getId()).get());
                break;
            case "Restaurants / Bars":
                RestaurantBar rb = restaurantBarRepository.findById(p.getId()).get();
                List<MenuProducts> mp = new ArrayList<MenuProducts>();
                model.addAttribute("place", rb);
                menuRepository.getMenuByRestauBar(rb).forEach(menu -> {
                    mp.add(new MenuProducts(menu, productRepository.getProductsByMenu(menu)));
                });
                model.addAttribute("menus", mp);
                break;
            case "Cinémas":
                Cinema c = cinemaRepository.findById(p.getId()).get();
                model.addAttribute("place", c);
                model.addAttribute("films", filmRepository.getAllFilmsByCinema(c));
                break;
            case "Parkings":
                Parking prk = parkingRepository.findById(p.getId()).get();
                model.addAttribute("place", p);
                model.addAttribute("price", parkingPriceRepository.getPriceOfParking(prk));
                break;
            case "Arrêts transports en commun":
                model.addAttribute("place", transportStopRepository.findById(p.getId()).get());
                break;
            default:
                return "redirect:/";
        }
        model.addAttribute("review", reviewRepository.getAllReviews(p));
        model.addAttribute("allCategories", categoryRepository.getAllCategories());
        return "place.html";
    }

    
    @RequestMapping("/setup/initDB")
    public String initDb(Model model) {
        initTestDatabase.main(); return "redirect:/";
    }
    
}
