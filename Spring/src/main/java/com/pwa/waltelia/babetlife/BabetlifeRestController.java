package com.pwa.waltelia.babetlife;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import com.pwa.waltelia.babetlife.entities.Category;
import com.pwa.waltelia.babetlife.entities.Cinema;
import com.pwa.waltelia.babetlife.entities.Film;
import com.pwa.waltelia.babetlife.entities.GPScoordinates;
import com.pwa.waltelia.babetlife.entities.Menu;
import com.pwa.waltelia.babetlife.entities.MenuProducts;
import com.pwa.waltelia.babetlife.entities.Park;
import com.pwa.waltelia.babetlife.entities.Parking;
import com.pwa.waltelia.babetlife.entities.ParkingPrice;
import com.pwa.waltelia.babetlife.entities.Place;
import com.pwa.waltelia.babetlife.entities.Product;
import com.pwa.waltelia.babetlife.entities.RestaurantBar;
import com.pwa.waltelia.babetlife.entities.Review;
import com.pwa.waltelia.babetlife.entities.Store;
import com.pwa.waltelia.babetlife.entities.TransportStop;
import com.pwa.waltelia.babetlife.entities.repositories.CategoryRepository;
import com.pwa.waltelia.babetlife.entities.repositories.CinemaRepository;
import com.pwa.waltelia.babetlife.entities.repositories.FilmRepository;
import com.pwa.waltelia.babetlife.entities.repositories.GPScoordinatesRepository;
import com.pwa.waltelia.babetlife.entities.repositories.MenuRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingPriceRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ParkingRepository;
import com.pwa.waltelia.babetlife.entities.repositories.PlaceRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ProductRepository;
import com.pwa.waltelia.babetlife.entities.repositories.RestaurantBarRepository;
import com.pwa.waltelia.babetlife.entities.repositories.ReviewRepository;
import com.pwa.waltelia.babetlife.entities.repositories.StoreRepository;
import com.pwa.waltelia.babetlife.entities.repositories.TransportStopRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BabetlifeRestController {
    
    @Inject
    InitTestDatabase initTestDatabase;

    @Inject
    CategoryRepository categoryRepository;

    @Inject
    PlaceRepository placeRepository;

    @Inject
    StoreRepository storeRepository;

    @Inject
    ParkRepository parkRepository;

    @Inject
    RestaurantBarRepository restaurantBarRepository;

    @Inject
    CinemaRepository cinemaRepository;

    @Inject
    ParkingRepository parkingRepository;

    @Inject
    ParkingPriceRepository parkingPriceRepository;

    @Inject
    TransportStopRepository transportStopRepository;

    @Inject
    MenuRepository menuRepository;

    @Inject
    ProductRepository productRepository;

    @Inject
    FilmRepository filmRepository;

    @Inject
    ReviewRepository reviewRepository;

    @Inject
    GPScoordinatesRepository GPScoordinatesRepository;

    @RequestMapping("/vue")
    public Map<String, List<Object>> getIndex() {
        Map<String, List<Object>> indexInfos = new HashMap<>();
        indexInfos.put("discover", new ArrayList<Object>(placeRepository.getRandomDiscovery()));
        List<TopCategory> topCategories = new ArrayList<>();
        categoryRepository.findAll().forEach(category -> {
            TopCategory topCategory = new TopCategory();
            topCategory.setTitle(category.getTitle());
            topCategory.setPlaces(placeRepository.getTopOfCategory(PageRequest.of(0, 2), category));
            topCategories.add(topCategory);
        });
        indexInfos.put("categories", new ArrayList<Object>(topCategories));
        indexInfos.put("allCategories", new ArrayList<Object>(categoryRepository.getAllCategories()));
        return indexInfos;
    }

    @RequestMapping("/vue/categorie/{categoryId}")
    public Map<String, List<Object>> getCategory(@PathVariable long categoryId) {
        Map<String, List<Object>> categoryInfos = new HashMap<>();
        try {
            Category c = categoryRepository.findById(categoryId).get();
            if (c.getTitle() == null) {
                return categoryInfos;
            }
            TopCategory placesByCateg = new TopCategory();
            placesByCateg.setTitle(c.getTitle());
            placesByCateg.setPlaces(placeRepository.getPlacesOfCategory(c));
            categoryInfos.put("placesCateg", new ArrayList<Object>(Collections.singletonList(placesByCateg)));
            categoryInfos.put("allCategories", new ArrayList<Object>(categoryRepository.getAllCategories()));

            return categoryInfos;
        } catch (NoSuchElementException e) {
            return categoryInfos;
        }
    }

    @RequestMapping("/vue/map")
    public Map<String, List<Object>> getMap() {
        Map<String, List<Object>> mapInfos = new HashMap<>();
        mapInfos.put("places", new ArrayList<Object>(StreamSupport.stream(placeRepository.findAll().spliterator(),false).collect(Collectors.toList())));
        mapInfos.put("categories", new ArrayList<Object>(StreamSupport.stream(categoryRepository.findAll().spliterator(),false).collect(Collectors.toList())));
        mapInfos.put("allCategories", new ArrayList<Object>(categoryRepository.getAllCategories()));
        return mapInfos;
    }

    @RequestMapping("/vue/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, Map<Category, List<Object>>> getAdmin() {
        Map<String, Map<Category, List<Object>>> adminInfos = new HashMap<>();
        Map<Category, List<Object>> categoriesWithPlaces = new HashMap<>();
        List<Category> categories = new ArrayList<Category>(categoryRepository.getAllCategories());
        categories.forEach((category) -> {
            categoriesWithPlaces.put(category, new ArrayList<Object>(StreamSupport.stream(placeRepository.findAll().spliterator(),false)
            .filter(place -> place.getCategory().getId() == category.getId())
            .collect(Collectors.toList())));
        });


        adminInfos.put("categoriesWithPlaces", categoriesWithPlaces);
        return adminInfos;
    }

    @RequestMapping("/vue/createParking")
    @PreAuthorize("hasRole('ADMIN')")
    public Long createParking(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        Parking p = null;
        Place associatedPlace = null;
        if (Long.valueOf(body.get("associatedPlace")) != -1L) {
            associatedPlace = placeRepository.findById(Long.valueOf(body.get("associatedPlace"))).get();
        }
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Parkings")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                p = parkingRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (p != null) {
                    GPScoordinates cooGPS = p.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    p.setCooGPS(cooGPS);
                    p.setName(body.get("name"));
                    p.setAddress(body.get("address"));
                    p.setNumTel(body.get("numTel"));
                    p.setOpeningHours(body.get("openHours"));
                    p.setNotation(Double.valueOf(body.get("notation")));
                    p.setCapacity(Integer.valueOf(body.get("capacity")));
                    p.setAssociatedPlace(associatedPlace);
                    parkingRepository.save(p);
                }
            } else {
                p = new Parking(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")), Integer.valueOf(body.get("capacity")), associatedPlace);
                parkingRepository.save(p);
            }
        }
        if (p == null) {
            return -1L;
        } else {
            return p.getId();
        }
    }

    @RequestMapping("/vue/createParkingPrice")
    @PreAuthorize("hasRole('ADMIN')")
    public void createParkingPrice(@RequestBody Map<String,String> body) {
        Parking p = parkingRepository.findById(Long.valueOf(body.get("parkingId"))).get();
        if (p != null) {
            if (body.get("changeOrCreate").equals("change")) {
                ParkingPrice parkingPrice = parkingPriceRepository.findById(Long.valueOf(body.get("parkingPriceId"))).get();
                if (parkingPrice != null) {
                    parkingPrice.setParking(p);
                    parkingPrice.setTime(Double.valueOf(body.get("time")));
                    parkingPrice.setPrice(Double.valueOf(body.get("price")));
                    parkingPriceRepository.save(parkingPrice);
                }
            } else if (body.get("changeOrCreate").equals("delete")) {
                parkingPriceRepository.deleteById(Long.valueOf(body.get("parkingPriceId")));
            } else {
                ParkingPrice parkingPrice = new ParkingPrice(p, Double.valueOf(body.get("time")), Double.valueOf(body.get("price")));
                parkingPriceRepository.save(parkingPrice);
            }
        }
    }

    @RequestMapping("/vue/createPark")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createPark(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        Park p = null;
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Parcs")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                p = parkRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (p != null) {
                    GPScoordinates cooGPS = p.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    p.setCooGPS(cooGPS);
                    p.setName(body.get("name"));
                    p.setAddress(body.get("address"));
                    p.setNumTel(body.get("numTel"));
                    p.setOpeningHours(body.get("openHours"));
                    p.setNotation(Double.valueOf(body.get("notation")));
                    parkRepository.save(p);
                }
            } else {
                p = new Park(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")));
                parkRepository.save(p);
            }
        }
        return p != null;
    }

    @RequestMapping("/vue/createMagasin")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createMagasin(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        Store s = null;
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Magasins")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                s = storeRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (s != null) {
                    GPScoordinates cooGPS = s.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    s.setCooGPS(cooGPS);
                    s.setName(body.get("name"));
                    s.setAddress(body.get("address"));
                    s.setNumTel(body.get("numTel"));
                    s.setOpeningHours(body.get("openHours"));
                    s.setNotation(Double.valueOf(body.get("notation")));
                    s.setType(body.get("type"));
                    storeRepository.save(s);
                }
            } else {
                s = new Store(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")), body.get("type"));
                storeRepository.save(s);
            }
        }
        return s != null;
    }

    @RequestMapping("/vue/createTransportStop")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createTransportStop(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        TransportStop t = null;
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Arrêts transports en commun")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                t = transportStopRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (t != null) {
                    GPScoordinates cooGPS = t.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    t.setCooGPS(cooGPS);
                    t.setName(body.get("name"));
                    t.setAddress(body.get("address"));
                    t.setNumTel(body.get("numTel"));
                    t.setOpeningHours(body.get("openHours"));
                    t.setNotation(Double.valueOf(body.get("notation")));
                    t.setLines(body.get("lines"));
                    transportStopRepository.save(t);
                }
            } else {
                t = new TransportStop(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")), body.get("lines"));
                transportStopRepository.save(t);
            }
        }
        return t != null;
    }

    @RequestMapping("/vue/createCinema")
    @PreAuthorize("hasRole('ADMIN')")
    public Long createCinema(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        Cinema cine = null;
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Cinémas")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                cine = cinemaRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (cine != null) {
                    GPScoordinates cooGPS = cine.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    cine.setCooGPS(cooGPS);
                    cine.setName(body.get("name"));
                    cine.setAddress(body.get("address"));
                    cine.setNumTel(body.get("numTel"));
                    cine.setOpeningHours(body.get("openHours"));
                    cine.setNotation(Double.valueOf(body.get("notation")));
                    cine.setNbRooms(Integer.valueOf(body.get("nbRooms")));
                    cinemaRepository.save(cine);
                }
            } else {
                cine = new Cinema(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")), Integer.valueOf(body.get("nbRooms")));
                cinemaRepository.save(cine);
            }
        }
        if (cine == null) {
            return -1L;
        } else {
            return cine.getId();
        }
    }

    @RequestMapping("/vue/createFilm")
    @PreAuthorize("hasRole('ADMIN')")
    public void createFilm(@RequestBody Map<String,String> body) throws ParseException {
        Cinema c = cinemaRepository.findById(Long.valueOf(body.get("cinemaId"))).get();
        Date d = new SimpleDateFormat("yyyy-MM-dd-HH:mm").parse(body.get("date").replace("T", "-"));
        if (c != null) {
            if (body.get("changeOrCreate").equals("change")) {
                Film film = filmRepository.findById(Long.valueOf(body.get("filmId"))).get();
                if (film != null) {
                    film.setCinema(c);
                    film.setTitle(body.get("title"));
                    film.setType(body.get("type"));
                    film.setPrice(Double.valueOf(body.get("price")));
                    film.setSchedule(d);
                    film.setTime(Double.valueOf(body.get("duration")));
                    film.setNumRoom(Integer.valueOf(body.get("numSalle")));
                    filmRepository.save(film);
                }
            } else if (body.get("changeOrCreate").equals("delete")) {
                filmRepository.deleteById(Long.valueOf(body.get("filmId")));
            } else {
                Film film = new Film(c, body.get("title"), body.get("type"), Double.valueOf(body.get("price")), d,
                    Double.valueOf(body.get("duration")), Integer.valueOf(body.get("numSalle")));
                filmRepository.save(film);
            }
        }
    }

    @RequestMapping("/vue/createRestaurantBar")
    @PreAuthorize("hasRole('ADMIN')")
    public Long createRestaurantBar(@RequestBody Map<String,String> body) {
        GPScoordinates gps = new GPScoordinates(Double.valueOf(body.get("latitude")), Double.valueOf(body.get("longitude")));
        GPScoordinatesRepository.save(gps);
        List<Category> c = new ArrayList<>();
        RestaurantBar rb = null;
        categoryRepository.getAllCategories().forEach(category -> {
            if (category.getTitle().equals("Restaurants / Bars")) {
                c.add(category);
            }
        });
        if (c.size() > 0) {
            if (body.get("changeOrCreate").equals("change")) {
                rb = restaurantBarRepository.findById(Long.valueOf(body.get("placeId"))).get();
                if (rb != null) {
                    GPScoordinates cooGPS = rb.getCooGPS();
                    cooGPS.setLatitude(Double.valueOf(body.get("latitude")));
                    cooGPS.setLongitude(Double.valueOf(body.get("longitude")));
                    GPScoordinatesRepository.save(cooGPS);
                    rb.setCooGPS(cooGPS);
                    rb.setName(body.get("name"));
                    rb.setAddress(body.get("address"));
                    rb.setNumTel(body.get("numTel"));
                    rb.setOpeningHours(body.get("openHours"));
                    rb.setNotation(Double.valueOf(body.get("notation")));
                    rb.setCapacity(Integer.valueOf(body.get("capacity")));
                    restaurantBarRepository.save(rb);
                }
            } else {
                rb = new RestaurantBar(c.get(0), body.get("name"), body.get("address"),
                    body.get("numTel"), gps, body.get("openHours"), Double.valueOf(body.get("notation")), Integer.valueOf(body.get("capacity")));
                restaurantBarRepository.save(rb);
            }
        }
        if (rb == null) {
            return -1L;
        } else {
            return rb.getId();
        }
    }

    @RequestMapping("/vue/createMenu")
    @PreAuthorize("hasRole('ADMIN')")
    public Long createMenu(@RequestBody Map<String,String> body) throws ParseException {
        RestaurantBar rb = restaurantBarRepository.findById(Long.valueOf(body.get("restaurantBarId"))).get();
        Menu menu = null;
        if (rb != null) {
            if (body.get("changeOrCreate").equals("change")) {
                menu = menuRepository.findById(Long.valueOf(body.get("menuId"))).get();
                if (menu != null) {
                    menu.setRestaurantBar(rb);
                    menu.setPrice(Double.valueOf(body.get("price")));
                    menuRepository.save(menu);
                }
            } else if (body.get("changeOrCreate").equals("delete")) {
                List<Product> products = productRepository.getProductsByMenu(menuRepository.findById(Long.valueOf(body.get("menuId"))).get());
                productRepository.deleteAll(products);
                menuRepository.deleteById(Long.valueOf(body.get("menuId")));
            } else {
                menu = new Menu(rb, Double.valueOf(body.get("price")));
                menuRepository.save(menu);
            }
        }
        if (menu == null) {
            return -1L;
        } else {
            return menu.getId();
        }
    }

    @RequestMapping("/vue/createProduit")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createProduit(@RequestBody Map<String,String> body) throws ParseException {
        Menu m = menuRepository.findById(Long.valueOf(body.get("menuId"))).get();
        Product produit = null;
        if (m != null) {
            if (body.get("changeOrCreate").equals("change")) {
                produit = productRepository.findById(Long.valueOf(body.get("productId"))).get();
                if (produit != null) {
                    produit.setMenu(m);
                    produit.setName(body.get("name"));
                    produit.setType(body.get("type"));
                    produit.setPrice(Double.valueOf(body.get("price")));
                    productRepository.save(produit);
                }
            } else if (body.get("changeOrCreate").equals("delete")) {
                productRepository.deleteById(Long.valueOf(body.get("productId")));
                return true;
            } else {
                produit = new Product(m, body.get("name"), body.get("type"), Double.valueOf(body.get("price")));
                productRepository.save(produit);
            }
        }
        return (produit != null);
    }

    @RequestMapping("/vue/removeAssociatedPlace")
    @PreAuthorize("hasRole('ADMIN')")
    public void removeAssociatedPlace(@RequestBody String id) {        
        Parking parking = parkingRepository.findById(Long.parseLong(id)).get();
        if (parking != null) {
            parking.setAssociatedPlace(null);
            parkingRepository.save(parking);
        }
        
    }

    @RequestMapping(value = "/vue/lieu/{placeId}")
    public Map<String, List<Object>> getPlace(@PathVariable long placeId) {
        Map<String, List<Object>> placeInfos = new HashMap<>();
        try {
            Place p = placeRepository.findById(placeId).get();
            switch (p.getCategory().getTitle()) {
                case "Magasins":
                    Store s = storeRepository.findById(p.getId()).get();
                    placeInfos.put("place", Collections.singletonList(s));
                    break;
                case "Parcs":
                    placeInfos.put("place", Collections.singletonList(parkRepository.findById(p.getId()).get()));
                    break;
                case "Restaurants / Bars":
                    RestaurantBar rb = restaurantBarRepository.findById(p.getId()).get();
                    List<MenuProducts> mp = new ArrayList<MenuProducts>();
                    placeInfos.put("place", Collections.singletonList(rb));
                    menuRepository.getMenuByRestauBar(rb).forEach(menu -> {
                        mp.add(new MenuProducts(menu, productRepository.getProductsByMenu(menu)));
                    });
                    placeInfos.put("menus", new ArrayList<Object>(mp));
                    break;
                case "Cinémas":
                    Cinema c = cinemaRepository.findById(p.getId()).get();
                    placeInfos.put("place", Collections.singletonList(c));
                    placeInfos.put("films", new ArrayList<Object>(filmRepository.getAllFilmsByCinema(c)));
                    break;
                case "Parkings":
                    Parking prk = parkingRepository.findById(p.getId()).get();
                    placeInfos.put("place", Collections.singletonList(p));
                    placeInfos.put("price", new ArrayList<Object>(parkingPriceRepository.getPriceOfParking(prk)));
                    break;
                case "Arrêts transports en commun":
                    placeInfos.put("place", Collections.singletonList(transportStopRepository.findById(p.getId()).get()));
                    break;
                default:
                    return placeInfos;
            }
            placeInfos.put("review", new ArrayList<Object>(reviewRepository.getAllReviews(p)));
            placeInfos.put("allCategories", new ArrayList<Object>(categoryRepository.getAllCategories()));
            return placeInfos;
        } catch (NoSuchElementException e) {
            return placeInfos;
        }
    }

    @RequestMapping("/vue/addReview")
    public boolean addReview(@RequestBody Map<String,String> body) {

        List<Place> p = new ArrayList<>();
        Review r = null;
        placeRepository.findAll().forEach( place -> {
            if ( place.getId().toString().equals(body.get("placeId"))){
                p.add(place);
            }
        });
        if (!p.isEmpty()) {
            if (!body.get("pseudo").isEmpty() && !body.get("comment").isEmpty() && !body.get("grade").isEmpty()) {
                r = new Review(p.get(0), body.get("comment"), Double.parseDouble(body.get("grade")), body.get("pseudo"));
                reviewRepository.save(r);
            }
        }
        return r != null;
    }


}
