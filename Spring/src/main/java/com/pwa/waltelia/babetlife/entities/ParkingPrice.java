package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class ParkingPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Parking parking;
    private double time;
    private double price;

    public ParkingPrice(Parking parking, double time, double price) {
        this.parking = parking;
        this.time = time;
        this.price = price;
    }

}
