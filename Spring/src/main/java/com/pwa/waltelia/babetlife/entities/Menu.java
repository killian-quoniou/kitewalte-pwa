package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private RestaurantBar restaurantBar;
    private double price;

    public Menu(RestaurantBar restaurantBar, double price) {
        this.restaurantBar = restaurantBar;
        this.price = price;
    }

}
