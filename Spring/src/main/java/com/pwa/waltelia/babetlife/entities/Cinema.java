package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Cinema extends Place {
    
    private int nbRooms;

    public Cinema(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String oepningHours, double notation, int nbRooms) {
        super(category, name, address, numTel, cooGPS, oepningHours, notation);
        this.nbRooms = nbRooms;
    }

    public int getNbRooms() {
        return nbRooms;
    }

    public void setNbRooms(int nbRooms) {
        this.nbRooms = nbRooms;
    }

}
