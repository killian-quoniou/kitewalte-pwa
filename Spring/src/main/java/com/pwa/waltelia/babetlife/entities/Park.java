package com.pwa.waltelia.babetlife.entities;

import javax.persistence.Entity;

import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class Park extends Place {

    public Park(Category category, String name, String address, String numTel, GPScoordinates cooGPS, String openingHours, double notation) {
        super(category, name, address, numTel, cooGPS, openingHours, notation);
    }

}
