package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Category;
import com.pwa.waltelia.babetlife.entities.Place;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PlaceRepository extends CrudRepository<Place, Long> {

    public List<Place> getRandomDiscovery();

    @Query("SELECT p FROM Place AS p WHERE p.category = :category ORDER BY p.notation DESC")
    public List<Place> getTopOfCategory(Pageable pageable, Category category);

    @Query("SELECT p FROM Place AS p WHERE p.category = :category")
    public List<Place> getPlacesOfCategory(Category category);

}
