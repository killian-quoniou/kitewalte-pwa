package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Menu;
import com.pwa.waltelia.babetlife.entities.RestaurantBar;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface MenuRepository extends CrudRepository<Menu, Long> {

    @Query("SELECT m FROM Menu AS m WHERE m.restaurantBar = :rb")
    public List<Menu> getMenuByRestauBar(RestaurantBar rb);
}   