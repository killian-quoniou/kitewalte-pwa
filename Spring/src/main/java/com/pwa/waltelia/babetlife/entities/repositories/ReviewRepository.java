package com.pwa.waltelia.babetlife.entities.repositories;

import java.util.List;

import com.pwa.waltelia.babetlife.entities.Review;
import com.pwa.waltelia.babetlife.entities.Place;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ReviewRepository extends CrudRepository<Review, Long> {

    @Query("SELECT r FROM Review AS r WHERE r.place = :l")
    public List<Review> getAllReviews(Place l);
}
