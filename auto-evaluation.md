# Auto-évoluation

## Temps passé pour chaque membres

Durant le projet, nous avons tenu à ce que le travail réalisé soit équitable entre tous les membres du groupe.

Nous avons globalement tous travaillé en moyenne 50 heures sur le projet.


## Répartition du travail

Concernant la répartition du travail, nous faisions, avant chaque Sprint, une réunion pour énumérer les fonctionnalités à réaliser durant ce dernier.

Ensuite, nous nous répartissions les tâches entres les différents membres, équitablement en fonction des préférences de chacun.

De plus, le membre assigné à une tâche pouvait être modifié en cours de Sprint.

Ainsi, grâce à cette organisation, la répartition du travail était naturellement équitable.

Nous avons tous travaillé sur le backend et le frontend. Il n'y a pas eu "d'équipe backend" ou "d'équipe frontend", car nous sommes tous curieux de comprendre comment fonctionne chaque coté d'une application fullstack.

L'utilisation de "merge request" au moment des mises à jour du dépôt GitLab, nous a permis de toujours avoir un œil sur ce qui était réalisé dans les tickets des autres membres du groupe.

## Conformité aux bonnes pratiques

- Au sujet de git:

    - commit/push souvent, pour ne pas avoir peur de faire des changements. 
        - **Note attribuée :** 20/20.
        - **Commentaire :** Nous avons, grâce aux différents Sprint que nous nous sommes imposés, développé régulièrement notre application et avons donc "commit" et "push" à la même fréquence.
    - utiliser des messages de commit qui décrivent le changement et surtout la raison du changement.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Nous avons toujours fait des messages de "commit" clairs et précis.
    - utiliser des messages en anglais.
        - **Note attribuée :** 17/20.
        - **Commentaire :** Nous avons parfois oublié que les "commits" se devaient d'être en anglais. Cependant, cela a été assez rare et n'est pas représentatif de la globalité des messages de "commits".
    - ne pas commiter les fichiers générés (utiliser un .gitignore de façon à ce que git status soit propre).
        - **Note attribuée :** 20/20.
        - **Commentaire :** Nous avons configuré dès le départ le fichier .gitignore pour que nous n'ayons pas de problème. Nous avons également directement mis à jour ce même fichier lorsque nous avons ajouté la partie Vue.JS au projet.
    - écrire les documents demandés en markdown (.md) pour qu’ils s’affichent correctement dans github.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Nous avons respecté cette consigne.
- Au sujet de votre code:

    - écrire votre code en anglais.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Tout le code est en anglais.
    - indenter/formatter votre code correctement.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Aucun problème d'intentation puisqu'on a immédiatement imposé une convention de code.
    - ne pas mélanger espaces et tabulations.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Nous avons choisi une convention entre nous dès le démarrage du projet pour garder une cohérence sur l'indentation.
    - garder votre code propre: pas de variables globales/statiques, choisir des noms correctement (packages, classes, etc), suivre les conventions (e.g., java convention), utiliser des constantes pour les valeurs constantes, …
        - **Note attribuée :** 15/20.
        - **Commentaire :** Au début du projet, nous avons manqué de rigueur sur l'arborescence de ce dernier, car nous ne savions pas quelles étaient les conventions à ce sujet. Pour éviter que le projet ne fonctionne plus, nous avons décidé de continuer avec la même structure tout le long de ce dernier. Ceci étant dû à notre perception traditionnelle qui est que le fonctionnement prévaut sur le reste et que ce n'était pas la priorité pour ce projet. Cependant, nous avons noté dans le document "retrospective.md" que nous ne referons plus cette erreur dans nos projets futurs.

- Vous écrivez du logiciel donc :

    - tester beaucoup et souvent.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Étant donné notre gestion de projet, chaque ajour devait être fonctionnel pour qu'un "merge" ne bloque pas la branche de développement, et donc les autres membres. Donc nous testions en permanence, plus précisément avant de "push" et après avoir "pull".
    - automatiser les tests qui peuvent l’être.
        - **Note attribuée :** /.
        - **Commentaire :** Nous ne pouvons pas nous noter sur ce point-là car nous ne savons pas vraiment à quoi correspondent des "tests automatisés". Nous avons seulement testé si ce que nous voulions faire pour chaque élément fonctionnait ou pas.
    - documenter comment utiliser, compiler, tester et lancer votre projet.
        - **Note attribuée :** 20/20.
        - **Commentaire :** Le document README.md répond à cette problématique.
    - documenter comment comprendre et reprendre votre projet.
        - **Note attribuée :** 15/20.
        - **Commentaire :** Nous avons utilisé des noms de fonctions suivant le principe "clean code" qui est que ces noms explicitent l'action que produit chacune de ces fonctions. Donc, un développeur souhaitant reprendre notre projet sera en mesure, s'il connait déjà le principe MVC, de continuer de développer le projet. Cependant, nous n'avons pas rédigé de documentation technique, par manque de temps et parce que ce n'était pas demandé pour ce projet.
