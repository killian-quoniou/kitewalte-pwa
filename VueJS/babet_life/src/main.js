import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import '@/js/bootstrap.bundle.min.js'
import '@/css/bootstrap.min.css'
import '@/css/ol.css'

createApp(App).use(router).mount('#app')
