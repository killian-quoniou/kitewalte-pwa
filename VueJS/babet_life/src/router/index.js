import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/categorie/:id',
    name: 'Category',
    component: () => import('../views/Category.vue')
  },
  {
    path: '/lieu/:id',
    name: 'Place',
    component: () => import('../views/Place.vue')
  },
  {
    path: '/carte',
    name: 'MapPage',
    component: () => import('../views/MapPage.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () => import('../views/Admin.vue')
  },
  {
    path: '/admin/chooseCategory',
    name: 'AdminChooseCategory',
    component: () => import('../views/AdminChooseCategory.vue')
  },
  {
    path: '/admin/createPlace/:changeOrCreate/:id/:placeId',
    name: 'AdminCreatePlace',
    component: () => import('../views/AdminCreatePlace.vue')
  },
  {
    path: '/admin/createParkingPrice/:changeOrCreate/:parkingId',
    name: 'AdminCreateParkingPrice',
    component: () => import('../views/AdminCreateParkingPrice.vue')
  },
  {
    path: '/admin/createFilm/:changeOrCreate/:cinemaId',
    name: 'AdminCreateFilm',
    component: () => import('../views/AdminCreateFilm.vue')
  },
  {
    path: '/admin/createMenu/:changeOrCreate/:restaurantBarId',
    name: 'AdminCreateMenu',
    component: () => import('../views/AdminCreateMenu.vue')
  },
  {
    path: '/admin/createProduit/:changeOrCreateMenu/:changeOrCreate/:restaurantBarId/:menuId',
    name: 'AdminCreateProduit',
    component: () => import('../views/AdminCreateProduit.vue')
  },
  {
    path: '/signin/:postSignup',
    name: 'Signin',
    component: () => import('../views/Signin.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('../views/Signup.vue')
  },
  {
    path: '/unAuthorizedPage',
    name: 'unauthorized',
    component: () => import('../views/UnauthorizedPage.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
